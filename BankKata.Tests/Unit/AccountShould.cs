﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankKata.Tests.Unit
{
    class AccountShould
    {


        Mock<ITransactionRepository> transactionRepositoryMock;
        Mock<IStatementPrinter> statementPrinterMock;

        Account account;


        [SetUp]
        public void SetUp() {

            transactionRepositoryMock = new Mock<ITransactionRepository>();
            statementPrinterMock = new Mock<IStatementPrinter>();

            account = new Account(
                transactionRepositoryMock.Object,
                statementPrinterMock.Object);
        }


        [Test]
        public void Store_a_deposit_transaction() { 
            
            // Act
            account.Deposit(100);

            // Assert
            transactionRepositoryMock.Verify(m => m.AddDeposit(100));
        }


        [Test]
        public void Store_a_withdrawal_transaction()
        {

            // Act
            account.Withdraw(100);

            // Assert
            transactionRepositoryMock.Verify(m => m.AddWithdrawal(100));
        }


        [Test]
        public void Print_an_statement()
        {
            // Arrange
            List<Transaction> transactions = new List<Transaction>();

            transactionRepositoryMock
                .Setup(m => m.AllTransactions())
                .Returns(transactions);

            // Act
            account.PrintStatement();

            // Assert
            statementPrinterMock.Verify(m => m.Print(transactions));
        }
    }
}
