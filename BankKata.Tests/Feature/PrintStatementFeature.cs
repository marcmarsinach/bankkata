﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankKata.Tests.Feature
{
    public class PrintStatementFeature
    {


        Mock<IConsole> consoleMock;

        Account account;


        [SetUp]
        public void SetUp() {
            consoleMock = new Mock<IConsole>();

            var transactionRepository = new TransactionRepository();
            var statementPrinter = new StatementPrinter();
            account = new Account(transactionRepository, statementPrinter);
        }


        [Test]
        public void Print_statement_containing_all_transactions() { 
            // Arrange

            // Act
            account.Deposit(1000);
            account.Withdraw(100);
            account.Deposit(500);

            account.PrintStatement();

            // Assert
            consoleMock.Verify(m => m.PrintLine("DATE | AMOUNT | BALANCE"));
            consoleMock.Verify(m => m.PrintLine("10/04/2014 | 500.00 | 1400.00"));
            consoleMock.Verify(m => m.PrintLine("02/04/2014 | -100 | 900.00"));
            consoleMock.Verify(m => m.PrintLine("01/04/2014 | 1000.00 | 1000.00"));
        }
    }
}
