﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankKata
{
    public interface IStatementPrinter
    {
        void Print(List<Transaction> transactions);
    }
}
