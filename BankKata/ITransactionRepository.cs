﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankKata
{
    public interface ITransactionRepository
    {
        void AddDeposit(int amount);

        void AddWithdrawal(int amount);

        List<Transaction> AllTransactions();
    }
}
